#!/usr/bin/env python3
# coding=utf-8

# ***** BEGIN LICENSE BLOCK *****
# (C) Copyright 2015-2021 Paolo Greppi simevo s.r.l.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ***** END LICENSE BLOCK ***** */

""" html2rss:
    scrap the homepage of a news site and generate a RSS 2.0.1 feed

    usage:
    html2rss (chenected|easme) > feed.rss"""

import sys
import datetime

# http://stackoverflow.com/posts/9441191
# apt-get install python-lxml python-beautifulsoup
import lxml.html.soupparser

# apt-get install python-feedgenerator
import feedgenerator

# apt-get install python-requests
import requests

if len(sys.argv) < 2:
    sys.exit("Usage: %s newsname(one of: chenected, easme)" % sys.argv[0])

if sys.argv[1] == "chenected":
    url = "http://www.aiche.org/chenected"  # the URL of the original new page
    baseurl = "http://www.aiche.org"  # the base url for all links
    feed_title = "AIChE ChEnected"  # the feed title will appear in the generated RSS
    feed_link = (
        "http://libpf.com/chenected2.rss"  # the public address of the scrapped newsfeed
    )
    # xpath strings to locate the list pf posts and within each post the required fields
    xpath_posts = "//article"
    xpath_title = "header/h3/a"
    xpath_link = "header/h3/a/@href"
    xpath_date = '*/div[@class = "attribution"]/time'
    xpath_abstract = 'div[@class = "content clearfix"]/div/div/div'
    xpath_author = '*/div[@class = "attribution"]/a'
    date_format = "%b %d, %Y"
elif sys.argv[1] == "easme":
    url = "http://ec.europa.eu/easme/en/news"  # the URL of the original new page
    baseurl = "http://ec.europa.eu/"  # the base url for all links
    feed_title = "EASME news"  # the feed title will appear in the generated RSS
    feed_link = (
        "http://libpf.com/easme.rss"  # the public address of the scrapped newsfeed
    )
    # xpath strings to locate the list pf posts and within each post the required fields
    xpath_posts = '//*[@id="block-system-main"]/div/div/div[2]/div'
    xpath_title = "div[1]/span/h2/a"
    xpath_link = "div[1]/span/h2/a/@href"
    xpath_date = "div[1]/span/div[1]/p[2]"
    xpath_abstract = "div[2]/div"
    xpath_author = ""
    date_format = "%d/%m/%Y"
else:
    sys.exit("unknown newsname %s" % sys.argv[1])

r = requests.get(url)
if r.status_code != 200:
    exit

html = r.text
tree = lxml.html.soupparser.fromstring(html)
matches = tree.xpath(xpath_posts)
articles = []
for match in matches:
    try:
        title = match.xpath(xpath_title)[0].text_content()
        link = baseurl + match.xpath(xpath_link)[0]
        date_input = match.xpath(xpath_date)[0].text_content()
        date = datetime.datetime.strptime(date_input, date_format)
        abstract = match.xpath(xpath_abstract)[0].text_content()
        if xpath_author == "":
            author = ""
        else:
            author = match.xpath(xpath_author)[0].text_content()
        articles.append(
            {
                "title": title,
                "abstract": abstract,
                "date": date,
                "link": link,
                "author": author,
            }
        )
    except lxml.etree.XPathEvalError:
        continue
    except IndexError:
        continue

# valid generators: Rss201rev2Feed, Atom1Feed
feed = feedgenerator.Rss201rev2Feed(
    title=feed_title,
    link=feed_link,
    description="Hourly scrapped for you from " + url,
    language="en",
)

for article in articles:
    feed.add_item(
        title=article["title"],
        link=article["link"],
        description=article["abstract"],
        author_name=article["author"],
        pubdate=article["date"],
    )

print(feed.writeString("utf-8"))
